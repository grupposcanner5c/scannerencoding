﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace SerialPortReader
{
    class MSRController
    {
        SerialPort port;
        private bool semaforo = false;

        #region costruttori
        public MSRController() //looks for the scanner by itself
        {

            OpenPort();
        }

        public MSRController(string portName) //uses a specificated scanner port. Throws Exception if not available
        {
            port = new SerialPort(portName);
            OpenPort();
        }

        public MSRController(SerialPort port) //uses the serial port given. The port must be closed or Exception is thrown
        {
            this.port = port;
            OpenPort();
        }
        #endregion

        private void OpenPort()
        {
            port.Open();
            FindMSR();
        }

        private void FindMSR() //finds the device port 
        {
            

        }

        public void SetSemaforo(bool semaforo)
        {
            this.semaforo = semaforo;
        }

        protected void ResetMSR()
        {
            byte[] buff;
            buff = StringToByteArray("1B61");
            port.Write(buff, 0, buff.Length);
        }

        protected byte[] TestMSR()
        {
            byte[] buff;
            buff = StringToByteArray("1B65");
            port.Write(buff, 0, buff.Length);

            Thread.Sleep(100);
            
            return WaitForInput();
        }

        public void SwitchLights(bool red = false, bool green = false, bool yellow = false)
        {
            byte[] buff;
            buff = StringToByteArray("1B81");   //turn off all the lights
            port.Write(buff, 0, buff.Length);
            if (green)
            {
                buff = StringToByteArray("1B83");   //turn on green light
                port.Write(buff, 0, buff.Length);
            }
            if (red)
            {
                buff = StringToByteArray("1B85");   //turn on red light
                port.Write(buff, 0, buff.Length);
            }
            if (yellow)
            {
                buff = StringToByteArray("1B84");   //turn on yellow light
                port.Write(buff, 0, buff.Length);
            }

        }

        public String[] ReadFromCard()
        {
            List<String> ret = new List<string>();
            if (!semaforo)
            {
                ResetMSR();

                byte[] buff;
                buff = StringToByteArray("1B72");
                port.Write(buff, 0, buff.Length);
                buff = WaitForInput();

                

                byte[] parse = buff.Skip(4).Take(buff.Length - 4).ToArray();

                try
                {
                    int tmpIndex = Array.IndexOf(parse, StringToByteArray("1B")[0]);
                    Console.WriteLine(tmpIndex);
                    string field1 = Encoding.ASCII.GetString(parse.Take(tmpIndex).ToArray());
                    ret.Add(field1.Substring(1, field1.Length - 2));

                    parse = parse.Skip(tmpIndex + 2).ToArray();
                    tmpIndex = Array.IndexOf(parse, StringToByteArray("1B")[0]);
                    string field2 = Encoding.ASCII.GetString(parse.Take(tmpIndex).ToArray());
                    ret.Add(field2.Substring(1, field2.Length - 2));

                    parse = parse.Skip(tmpIndex + 2).ToArray();
                    tmpIndex = Array.IndexOf(parse, StringToByteArray("3F")[0]) + 1;
                    string field3 = Encoding.ASCII.GetString(parse.Take(tmpIndex).ToArray());
                    ret.Add(field3.Substring(1, field3.Length - 2));


                    Console.WriteLine(BitConverter.ToString(parse.Skip(tmpIndex + 2).ToArray()).Replace("-", ""));

                    if (BitConverter.ToString(parse.Skip(tmpIndex + 2).ToArray()).Replace("-", "") != "1B30")
                    {
                        throw new ReadException();
                    }
                    else
                    {
                        Console.WriteLine("Operation ended succesfully!");
                    }
                }
                catch (ArgumentOutOfRangeException) { }



                //Console.WriteLine(field1);
                //Console.WriteLine(field2);
                //Console.WriteLine(field3);

                //String[] ret =
                //{
                //    field1.Substring(1, field1.Length - 2),
                //    field2.Substring(1, field2.Length - 2),
                //    field3.Substring(1, field3.Length - 2)
                //};

                Console.WriteLine("Card: ");
                foreach (string write in ret)
                {
                    Console.WriteLine(write);
                }
            }
            return ret.ToArray();


        }

        public String[] AdminReadFromCard()
        {
            SetSemaforo(true);

            ResetMSR();

            byte[] buff;
            buff = StringToByteArray("1B72");
            port.Write(buff, 0, buff.Length);
            buff = AdminWaitForInput();
            SetSemaforo(false);
            List<String> ret = new List<string>();

            byte[] parse = buff.Skip(4).Take(buff.Length - 4).ToArray();

            try
            {
                int tmpIndex = Array.IndexOf(parse, StringToByteArray("1B")[0]);
                Console.WriteLine(tmpIndex);
                string field1 = Encoding.ASCII.GetString(parse.Take(tmpIndex).ToArray());
                ret.Add(field1.Substring(1, field1.Length - 2));

                parse = parse.Skip(tmpIndex + 2).ToArray();
                tmpIndex = Array.IndexOf(parse, StringToByteArray("1B")[0]);
                string field2 = Encoding.ASCII.GetString(parse.Take(tmpIndex).ToArray());
                ret.Add(field2.Substring(1, field2.Length - 2));

                parse = parse.Skip(tmpIndex + 2).ToArray();
                tmpIndex = Array.IndexOf(parse, StringToByteArray("3F")[0]) + 1;
                string field3 = Encoding.ASCII.GetString(parse.Take(tmpIndex).ToArray());
                ret.Add(field3.Substring(1, field3.Length - 2));


                Console.WriteLine(BitConverter.ToString(parse.Skip(tmpIndex + 2).ToArray()).Replace("-", ""));
                
                if (BitConverter.ToString(parse.Skip(tmpIndex + 2).ToArray()).Replace("-", "") != "1B30")
                {
                    throw new ReadException();
                }
                else
                {
                    Console.WriteLine("Operation ended succesfully!");
                }
            }
            catch (ArgumentOutOfRangeException) { }



            //Console.WriteLine(field1);
            //Console.WriteLine(field2);
            //Console.WriteLine(field3);

            //String[] ret =
            //{
            //    field1.Substring(1, field1.Length - 2),
            //    field2.Substring(1, field2.Length - 2),
            //    field3.Substring(1, field3.Length - 2)
            //};

            Console.WriteLine("Card: ");
            foreach (string write in ret)
            {
                Console.WriteLine(write);
            }

            return ret.ToArray();

        }

        public void WriteToCard(string field1, string field2="", string field3="")
        {
            if (!semaforo)
            {
                ResetMSR();
                byte[] buff;
                buff = StringToByteArray("1B6F080808"); //set the parameters to 8bits strings
                port.Write(buff, 0, buff.Length);
                Console.WriteLine(BitConverter.ToString(WaitForInput()).Replace("-", ""));

                SwitchLights(false, false, true);
                List<byte> write = new List<byte>();
                write.AddRange(StringToByteArray("1B77"));
                write.AddRange(StringToByteArray("1B73"));

                #region write fields
                //write field 1
                write.AddRange(StringToByteArray("1B01"));
                write.AddRange(Encoding.UTF8.GetBytes(field1.ToUpper()));

                if (field2 != "")    //write field 2
                {
                    write.AddRange(StringToByteArray("1B02"));
                    write.AddRange(Encoding.UTF8.GetBytes(field2.ToUpper()));
                }

                if (field3 != "")   //write field 3
                {
                    write.AddRange(StringToByteArray("1B03"));
                    write.AddRange(Encoding.UTF8.GetBytes(field3.ToUpper()));
                }
                #endregion

                write.AddRange(StringToByteArray("3F1C"));
                port.Write(write.ToArray(), 0, write.ToArray().Length);

                if (BitConverter.ToString(WaitForInput()).Replace("-", "") != "1B30")
                {
                    throw new WriteException();
                }
                else
                {
                    Console.WriteLine("Operation ended succesfully!");
                }
            }

        }

        public void AdminWriteToCard(string field1, string field2="", string field3="")
        {
            SetSemaforo(true);
            ResetMSR();
            byte[] buff;
            buff = StringToByteArray("1B6F080808"); //set the parameters to 8bits strings
            port.Write(buff, 0, buff.Length);
            Console.WriteLine(BitConverter.ToString(AdminWaitForInput()).Replace("-", ""));

            SwitchLights(false, false, true);
            List<byte> write = new List<byte>();
            write.AddRange(StringToByteArray("1B77"));
            write.AddRange(StringToByteArray("1B73"));

            #region write fields
            //write field 1
            write.AddRange(StringToByteArray("1B01"));
            write.AddRange(Encoding.UTF8.GetBytes(field1.ToUpper()));

            if (field2 != "")    //write field 2
            {
                write.AddRange(StringToByteArray("1B02"));
                write.AddRange(Encoding.UTF8.GetBytes(field2.ToUpper()));
            }

            if (field3 != "")   //write field 3
            {
                write.AddRange(StringToByteArray("1B03"));
                write.AddRange(Encoding.UTF8.GetBytes(field3.ToUpper()));
            }
            #endregion

            write.AddRange(StringToByteArray("3F1C"));
            port.Write(write.ToArray(), 0, write.ToArray().Length);

            if (BitConverter.ToString(AdminWaitForInput()).Replace("-", "") != "1B30")
            {
                SetSemaforo(false);
                throw new WriteException();
            }
            else
            {
                SetSemaforo(false);
                Console.WriteLine("Operation ended succesfully!");
            }
            

        }

        public void EraseCardsData(bool field1=true, bool field2=true, bool field3=true)
        {
            if (!semaforo)
            {
                ResetMSR();
                List<byte> delete = new List<byte>();
                delete.AddRange(StringToByteArray("1B63"));
                if (field1 && !field2 && !field3)
                {
                    delete.AddRange(StringToByteArray("00"));
                }
                else if (!field1 && field2 && !field3)
                {
                    delete.AddRange(StringToByteArray("02"));
                }
                else if (!field1 && !field2 && field3)
                {
                    delete.AddRange(StringToByteArray("04"));
                }
                else if (field1 && field2 && !field3)
                {
                    delete.AddRange(StringToByteArray("03"));
                }
                else if (!field1 && field2 && field3)
                {
                    delete.AddRange(StringToByteArray("06"));
                }
                else if (field1 && !field2 && field3)
                {
                    delete.AddRange(StringToByteArray("05"));
                }
                else if (field1 && field2 && field3)
                {
                    delete.AddRange(StringToByteArray("07"));
                }
                port.Write(delete.ToArray(), 0, delete.ToArray().Length);

                if (BitConverter.ToString(WaitForInput()).Replace("-", "") != "1B30")
                {
                    throw new EraseException();
                }
                else
                {
                    Console.WriteLine("Operation ended succesfully!");
                }
            }
        }

        public void AdminEraseCardsData(bool field1=true, bool field2=true, bool field3=true)
        {
            SetSemaforo(true);
            ResetMSR();
            List<byte> delete = new List<byte>();
            delete.AddRange(StringToByteArray("1B63"));
            if(field1 && !field2 && !field3)
            {
                delete.AddRange(StringToByteArray("00"));
            }
            else if (!field1 && field2 && !field3)
            {
                delete.AddRange(StringToByteArray("02"));
            }
            else if (!field1 && !field2 && field3)
            {
                delete.AddRange(StringToByteArray("04"));
            }
            else if (field1 && field2 && !field3)
            {
                delete.AddRange(StringToByteArray("03"));
            }
            else if (!field1 && field2 && field3)
            {
                delete.AddRange(StringToByteArray("06"));
            }
            else if (field1 && !field2 && field3)
            {
                delete.AddRange(StringToByteArray("05"));
            }
            else if (field1 && field2 && field3)
            {
                delete.AddRange(StringToByteArray("07"));
            }
            port.Write(delete.ToArray(), 0, delete.ToArray().Length);

            if (BitConverter.ToString(AdminWaitForInput()).Replace("-", "") != "1B30")
            {
                SetSemaforo(false);
                throw new EraseException();
            }
            else
            {
                SetSemaforo(false);
                Console.WriteLine("Operation ended succesfully!");
            }
        }

        private byte[] StringToByteArray(string hex)
        {
            //Console.WriteLine("\n" + hex + "\n");
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        private byte[] WaitForInput()
        {
            byte[] buff = new byte[0];

            bool tmpBool = true;
            while (tmpBool)
            {
                if (port.BytesToRead > 0)
                {
                    Thread.Sleep(200);
                    int btr = port.BytesToRead;
                    buff = new byte[btr];
                    port.Read(buff, 0, btr);
                    Console.WriteLine("Wait for input: " + BitConverter.ToString(buff).Replace("-",""));
                    tmpBool = false;
                }
                else if(semaforo){
                    throw new SemaforoException();
                }
            }
            return buff;
        }

        private byte[] AdminWaitForInput()
        {
            byte[] buff = new byte[0];

            bool tmpBool = true;
            while (tmpBool)
            {
                if (port.BytesToRead > 0)
                {
                    Thread.Sleep(200);
                    int btr = port.BytesToRead;
                    buff = new byte[btr];
                    port.Read(buff, 0, btr);
                    Console.WriteLine("Wait for input: " + BitConverter.ToString(buff).Replace("-",""));
                    tmpBool = false;
                }
                else
                {
                    //Console.Write(".");
                }
            }
            return buff;
        }

        
    }

    public class SemaforoException : ScannerException { }

    public class ScannerException : Exception { }

    public class ReadException : ScannerException { }

    public class WriteException : ScannerException { }

    public class EraseException : ScannerException { }
}
