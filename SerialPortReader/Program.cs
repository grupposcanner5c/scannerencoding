﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

//(usb.dst == "1.9.0" || usb.dst == "1.9.1" || usb.dst == "1.9.2" || usb.dst == "1.9.3" && usb.src=="host") || (usb.dst == "host" && usb.src=="1.9.0" ||  usb.src=="1.9.1" ||  usb.src=="1.9.2" ||  usb.src=="1.9.3")
//filtro da applicare a packet tracer;

    //Commento da eliminare

namespace SerialPortReader
{

    class MyThread
    {
        SerialPort port;
        public MyThread(SerialPort port)
        {
            this.port = port;
        }

        public void MyMethod()
        {
            while (true)
            {
                byte[] bytes;
                
                if (port.BytesToRead>0)
                {
                    int b = port.BytesToRead;
                    bytes = new byte[b];
                    int i = port.Read(bytes, 0, b);
                    Console.Write(ByteArrayToString(bytes));
                }
            }
        }

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            MSRController controller = new MSRController("COM3");

            Console.WriteLine("Inizialized");

            //controller.WriteToCard("ciao","1234","5678");
            controller.ReadFromCard();

            Console.WriteLine("Finito!");
            Console.ReadKey();
        }
        
    }
}
