﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialPortReader
{
    class Converter
    {
        private String data;
        private byte[] result;

        public byte[] Result
        {
            get { return result; }
        }
        
        public Converter(String data)
        {
            this.data = "%" + data.ToUpper() + "?";
            char[,] tmp = conversion();
            tmp = format<char>(tmp, '0');
            result = bidimensionalToByteMono(tmp);
            //Console.WriteLine(result.Length);
        }

        private byte[] bidimensionalToByteMono(char[,] vett)
        {
            StringBuilder sb = new StringBuilder();
            for (int i=0;i<vett.GetLength(1); i++)
            {
                
                for(int j = 0; j < vett.GetLength(0); j++)
                {
                    if (vett[j, i] != '\0')
                    {
                        sb.Append(vett[j, i]);
                    }
                    else
                        break;
                    
                }
            }
            string binaryStr = sb.ToString();

            var byteArray = Enumerable.Range(0, int.MaxValue / 8)
                          .Select(i => i * 8)    // get the starting index of which char segment
                          .TakeWhile(i => i < binaryStr.Length)
                          .Select(i => binaryStr.Substring(i, 8)) // get the binary string segments
                          .Select(s => Convert.ToByte(s, 2)) // convert to byte
                          .ToArray();


            return byteArray;
        }

        private T[,] format<T>(T[,] vett, T fill)
        {
            int rows = 7 * vett.GetLength(1);
            T[,] res = new T[8, rows % 8 == 0 ? ((rows * 7) / 8) : ((rows * 7) / 8) - 1];
            int j = 0, k = 0;
            for(int i = 0; i < vett.GetLength(1); i++)
            {
                foreach(T tmp in GetRow<T>(vett, i))
                {
                    res[j, k] = tmp;
                    j++;
                    if (j > 7)
                    {
                        j = 0;
                        k++;
                    }
                }
            }
            for (; j < 8 && j != 0; j++)
            {
                res[j, k] = fill;
            }
            return res;
        }

        private char[,] conversion()
        {
            char[,] res = new char[7,data.Length+1];
            char[] chars = data.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {

                string binary = Convert.ToString(chars[i], 2).PadLeft(8, '0');
                char[] tmp = binary.ToCharArray();
                res[0, i] = tmp[7];
                res[1, i] = tmp[6];
                res[2, i] = tmp[5];
                res[3, i] = tmp[4];
                res[4, i] = tmp[3];
                res[5, i] = tmp[1];
                res[6, i] = parity(GetRow<char>(res, i), res.GetLength(0)-1, false);        //Errore: getRow restituisce al colonna. GetColumn la riga (non chiedetemi il perche')
                //Console.WriteLine(binary);
                
            }
            for(int i = 0; i < res.GetLength(0)-1; i++)
            {
                res[i, res.GetLength(1) - 1] = parity(GetColumn<char>(res, i), res.GetLength(1) - 1, true);
            }
            res[res.GetLength(0) - 1, res.GetLength(1) - 1] = parity(GetRow<char>(res, res.GetLength(1) - 1), res.GetLength(0) - 1, false);
            

            return res;
        }
        


        private char parity(char[] vett, int length, bool parity)
        {
            //1 dispari = 0
            int uno = 0;
            for (int i = 0; i < length; i++)
            {
                if (vett[i] == '1') 
                {
                    uno++;
                }
            }
            char ret = ' ';
            switch (parity)
            {
                case true:
                    ret = uno % 2 == 0 ? '0' : '1';
                    break;

                case false:
                    ret = uno % 2 == 0 ? '1' : '0';
                    break;
            }
            return ret;
        }



        private char[] verticalParity(char[,] vett) 
        {
            //1 pari = 0
            char[] row = new char[7];
            int uno = 0;
            for (int i = 0; i < vett.GetLength(0); i++)
            {
                for (int j = 0; j < vett.GetLength(1)-1; j++)
                {
                    if (vett[j, i] == '1')
                    {
                        uno++;
                    }
                }
                row[i] = uno % 2 == 0 ? '1' : '0';
                uno = 0;
            }
            //row[6] = horizontalDisparity(GetRow<char>(vett, vett.GetLength(1)-2));
            return row;
        }

        

        private T[] GetColumn<T>(T[,] matrix, int columnNumber)
        {
            T[] ret = new T[matrix.GetLength(1)];

            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = matrix[columnNumber, i];
            }
            return ret;
        }



        private T[] GetRow<T>(T[,] matrix, int rowNumber)
        {
            T[] ret = new T[matrix.GetLength(0)];

            for(int i = 0; i < ret.Length; i++)
            {
                ret[i] = matrix[i, rowNumber];
            }
            return ret;
        }



        private T[,] insertRow<T>(T[,] matrix, T[] array, int row)
        {
            for(int i = 0; i < array.Length; i++)
            {
                matrix[i, row] = array[i];
            }
            return matrix;
        }


        

    }

}
