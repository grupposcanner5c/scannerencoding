﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialPortReader
{
    class DeConverter
    {
        byte[] data;
        public string finalString;


        public DeConverter(byte[] data)
        {
            this.data = data;
            run();
        }

        private void run()
        {
            string res = "";
            foreach (byte tmpb in data)
            {
                res += Convert.ToString(tmpb, 2).PadLeft(8, '0');
                //Console.WriteLine("1." + res);
            }
            //Console.WriteLine(res);
            res = res.Substring(0, res.Length - (res.Length % 7));
            //Console.WriteLine(res);
            ParityCheck(res);
            finalString = originalString(res);
        }

        private void ParityCheck(string strToCheck)
        {
            #region ------------------------------------------------matrice--------------------------------------------------
            char[,] matrix = new char[7,strToCheck.Length/7];
            int c = 0;
            foreach (char ch in strToCheck)
            {
                Console.Write(ch);
                matrix[c % 7, c / 7] = ch;
                if(++c % 7 == 0)
                {
                    Console.WriteLine();
                }
                
            }
            #endregion

            #region ----------------------------------------------Parita' verticale----------------------------------------------------
            Console.WriteLine("Parita' verticale");
            for(int i=0; i < 7; i++)
            {
                c = 0;
                for(int j=0; j<matrix.GetLength(1)-1; j++)
                {
                    //Console.Write(matrix[i, j]);
                    if (matrix[i,j] == '1')
                    {
                        c = c == 1 ? 0 : 1;
                    }
                }
                Console.WriteLine(c + "\t" + matrix[i, matrix.GetLength(1) - 1]);
                if (c != int.Parse(matrix[i, matrix.GetLength(1)-1].ToString()))
                {
                    throw new Exception();
                }
            }
            #endregion

            #region ----------------------------------------------Disparita' orizzontale----------------------------------------------------
            Console.WriteLine("Disparita' orizzontale");
            for(int i = 0; i < matrix.GetLength(1); i++)
            {
                c = 0;
                for (int j=0; j < 6; j++)
                {
                    if (matrix[j, i] == '1')
                    {
                        //Console.Write(matrix[j, i]);
                        c = c == 1 ? 0 : 1;
                    }
                }
                Console.WriteLine(c + "\t" + matrix[6, i]);
                if (c == int.Parse(matrix[6,i].ToString()))
                {
                    throw new Exception();
                }
            }
            #endregion
        }

        private string originalString(string strToConvert)
        {
            string ret = "";
            for(int i=0; i<(strToConvert.Length/7)-1 ; i++)
            {
                var tmp = strToConvert.Substring(7 * i, 6).ToCharArray();

                //    newStr[j] = tmp.ToCharArray()[1] == '0' ? '1' : '0';
                string arrayTmp = ('0' + tmp[5].ToString() + (tmp[5] == '0' ? '1' : '0') + tmp[4] + tmp[3] + tmp[2] + tmp[1] + tmp[0]);
                //Console.WriteLine(arrayTmp);
                ret += arrayTmp;
            }
            var byteArray = Enumerable.Range(0, int.MaxValue / 8)
                          .Select(i => i * 8)    // get the starting index of which char segment
                          .TakeWhile(i => i < ret.Length)
                          .Select(i => ret.Substring(i, 8)) // get the binary string segments
                          .Select(s => Convert.ToByte(s, 2)) // convert to byte
                          .ToArray();
            return Encoding.Default.GetString(byteArray);
        }
    }
}
